import { useEffect, useMemo, useRef, useState } from "react"

function UseMemoExample() {
    const [number, setNumber] = useState(1)
    const [inc, setInc] = useState(0)

    // UseMemo helps to cache the value generated to help optimize performance without recalculating every single time
    const sqrt = useMemo(() => getSqrt(number), [number])

    const renders = useRef(1)

    const onClick = () => {
        setInc((prevState) => {
            console.log(prevState + 1)
            return prevState + 1
        })
    }

    useEffect(() => {
        renders.current = renders.current + 1
    })

  return (
    <div>
        <input type="number" className="input-control w-25" 
        value={number} onChange={(e) => setNumber(e.target.value)}/>

        <h2 className="my-3">The square root of {number} is {sqrt}</h2>

        <button className="btn-primary" onClick={onClick}>Re-render</button>
        <h1>Renders: {renders.current}</h1>
    </div>
  )
}

function getSqrt(n){
    for(let i = 0; i <= 10000; i++){
        console.log(i)
    }
    console.log("expensive function called!");
    return Math.sqrt(n)
}

export default UseMemoExample