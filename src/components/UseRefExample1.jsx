import { useRef } from "react"

// useRef() is a good way to get reference to DOM elements without modification to the existing state

function UseRefExample1() {
    const inputRef = useRef()
    const paraRef = useRef()

    const onSubmit = e => {
        e.preventDefault()
        console.log(inputRef.current.value)
        inputRef.current.value = "Hello"
        inputRef.current.style.background = "red"
        paraRef.current.innerText = "Goodbye"
    }


  return (
    <div>
        <form onSubmit={onSubmit}>
            <label htmlFor="name">Name</label>
            <input type="text" id="name" className="form-control mb-2" ref={inputRef}/>
            <button type="submit" className="btn btn-primary">submit</button>
        </form>
        <p ref={paraRef} onClick={() => inputRef.current.focus()}></p>
    </div>
  )
}

export default UseRefExample1